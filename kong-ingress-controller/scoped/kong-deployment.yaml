---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: kong
  name: ingress-kong
  namespace: kong-k8s-oidc
spec:
  replicas: 3
  selector:
    matchLabels:
      app: kong
  strategy:
    rollingUpdate:
      maxSurge: 3
      maxUnavailable: 0
    type: RollingUpdate
  template:
    metadata:
      annotations:
        prometheus.io/port: "9542"
        prometheus.io/scrape: "true"
      labels:
        app: kong
    spec:
      serviceAccountName: kong-serviceaccount
      containers:
        - name: proxy
          image: revomatico/docker-kong-oidc:1.4.0-1
          env:
            - name: KONG_PLUGINS
              value: bundled,oidc
            - name: KONG_DATABASE
              value: "off"
            - name: KONG_NGINX_HTTP_INCLUDE
              value: "/kong/servers.conf"
            - name: KONG_ADMIN_ACCESS_LOG
              value: /dev/stdout
            - name: KONG_ADMIN_ERROR_LOG
              value: /dev/stderr
            - name: KONG_ADMIN_LISTEN
              value: 0.0.0.0:8001, 0.0.0.0:8444 ssl
            - name: KONG_NGINX_HTTP_SET_DECODE_BASE64
              value: "$$session_secret kong"
            - name: KONG_LOG_LEVEL
              value: info
            - name: KONG_NGINX_HTTP_LARGE_CLIENT_HEADER_BUFFERS
              value: "4 64k"
            - name: KONG_NGINX_HTTP_PROXY_INTERCEPT_ERRORS
              value: "off"
          lifecycle:
            preStop:
              exec:
                command: ["/bin/sh", "-c", "kong quit"]
          ports:
            - name: admin
              containerPort: 8001
            - name: admin-ssl
              containerPort: 8444
            - name: proxy
              containerPort: 8000
              protocol: TCP
            - name: proxy-ssl
              containerPort: 8443
              protocol: TCP
            - name: metrics
              containerPort: 9542
              protocol: TCP
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /status
              port: 8001
              scheme: HTTP
            initialDelaySeconds: 30
            periodSeconds: 200
            successThreshold: 1
            timeoutSeconds: 2
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /status
              port: 8001
              scheme: HTTP
            periodSeconds: 20
            successThreshold: 1
            timeoutSeconds: 2
          volumeMounts:
            - name: kong-server-blocks
              mountPath: /kong
        - name: ingress-controller
          args:
            - /kong-ingress-controller
            # the kong URL points to the kong admin api server
            - --kong-url=http://localhost:8001
            - --admin-tls-skip-verify
            # Service from were we extract the IP address/es to use in Ingress status
            - --publish-service=kong-k8s-oidc/kong-proxy
            - --ingress-class=kong-k8s-oidc
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: metadata.namespace
          image: kong-docker-kubernetes-ingress-controller.bintray.io/kong-ingress-controller:0.6.1
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /healthz
              port: 10254
              scheme: HTTP
            periodSeconds: 20
            successThreshold: 1
            timeoutSeconds: 2
          readinessProbe:
            failureThreshold: 10
            httpGet:
              path: /healthz
              port: 10254
              scheme: HTTP
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 2
      volumes:
        - name: kong-server-blocks
          configMap:
            name: kong-server-blocks
